terraform {
  required_version = ">=1.2.3"
  required_providers {
    aws = ">=3.0.0"
  }
}

resource "aws_db_instance" "db-garde" {
  identifier                   = "Garde-db"
  engine                       = "sqlserver-web"
  engine_version               = "13.00.6300.2.v1"      # SQL Server Web 2016 (13.00.6404.1)
  instance_class               = "db.r5.xlarge"
  allocated_storage            = 100
  #db_name                      // Note that this does not apply for Oracle or SQL Server engines. 
  username                     = "admingarde"
  password                     = "XtfdkihfgrLHDxsdwer" #sensitive - look for better approuch
  port                         = 1433
  parameter_group_name         = "default.sqlserver-web-13.0"
  skip_final_snapshot          = true
  copy_tags_to_snapshot        = true
  #deletion_protection          = true
  multi_az                     = false
  availability_zone            = "us-east-1a"
  vpc_security_group_ids     = "sg-0ae425bec87b1452c"
  db_subnet_group_name         = ["VPC-Garde"]
  performance_insights_enabled = false

  tags = {
    name        = "Garde-db"
    Project     = "Garde_TS_AWS"
    Environment = "Prod"
    SquadBsp    = "Yoda"
  }
}
