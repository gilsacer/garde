resource "aws_vpn_gateway" "vpn_gateway" {
  vpc_id = "vpc-088f823c2ec4f1ad1"
}

resource "aws_customer_gateway" "primary" {
  bgp_asn    = 65000
  ip_address = "189.125.163.249"
  type       = "ipsec.1"

}

resource "aws_customer_gateway" "secondary" {
  bgp_asn    = 65000
  ip_address = "200.205.39.73"
  type       = "ipsec.1"

}

resource "aws_vpn_connection" "main" {
  vpn_gateway_id      = aws_vpn_gateway.vpn_gateway.id
  customer_gateway_id = aws_customer_gateway.customer_gateway.id
  type                = "ipsec.1"
  static_routes_only  = true
}
